/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicios;

import java.util.Scanner;

/**
 *
 * @author T-Gamer
 */
public class Exercicios {

    public static String A(String entrada) {

        char w;
        String retorno = "Exercicio a\n"
                + "E1 = {a, b} \n"
                + "L1 = {w  e E1* | w começa com a e termina com b }\n";
        boolean terminou = false;
        boolean erro = false;
        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA -> Condição não aceita";
            retorno = retorno + "ENTRADA: " + entrada + "\n";
        } else {
            while (!terminou && estado != 3) {
                w = entrada.charAt(pos);
                switch (estado) {
                    case 0:
                        if (w == 'a') {
                            estado = 1;
                            retorno = retorno + "q0 -> q1 \n";
                        } else if(w == 'b'){
                            estado = 3;
                            retorno = retorno + "q0 -> q3 \n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 1:
                        if (w == 'a') {
                            estado = 1;
                            retorno = retorno + "q1 -> q1 \n";
                        } else if(w == 'b'){
                            estado = 2;
                            retorno = retorno + "q1 -> q2 \n";
                        }else{
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 2:
                        if (w == 'a') {
                            estado = 1;
                            retorno = retorno + "q2 -> q1 \n";
                        } else if(w == 'b'){
                            estado = 2;
                            retorno = retorno + "q2 -> q2 \n";
                        }else{
                            terminou = true;
                            erro = true;
                        }
                        break;
                }
                if (erro == true){
                    return "Erro - caracteres não aceitos";
                }
                if (estado == 3) {
                    retorno = retorno + "q0 -> q3 (Condição de erro) \n";
                }
                pos++;
                if (pos == entrada.length()) {
                    terminou = true;
                }
            }
            if (estado == 2) {
                retorno = retorno + "Condição aceita";
            } else {
                retorno = retorno + "Condição não aceita";
            }
        }
        return retorno;
    }

    public static String B(String entrada) {

        char w;
        String retorno = "Exercicio b\n"
                + "E1 = {a, b} \n"
                + "L2 = {w  e E1* | w possui aaa como subcadeia }\n";
        boolean terminou = false;
        boolean erro = false;
        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA -> Condição não aceita";
            retorno = retorno + "ENTRADA: " + entrada + "\n";
        } else {
            while (!terminou) {
                w = entrada.charAt(pos);

                switch (estado) {
                    case 0:
                        if (w == 'a') {
                            estado = 1;
                            retorno = retorno + "q0 -> q1\n";
                        } else if(w == 'b'){
                            estado = 4;
                            retorno = retorno + "q0 -> q4\n";
                        } else{
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 1:
                        if (w == 'a') {
                            estado = 2;
                            retorno = retorno + "q1 -> q2\n";
                        } else if(w == 'b'){
                            estado = 0;
                            retorno = retorno + "q1 -> q0\n";
                        } else{
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 2:
                        if (w == 'a') {
                            estado = 3;
                            retorno = retorno + "q2 -> q3\n";
                        } else if(w == 'b') {
                            estado = 0;
                            retorno = retorno + "q2 -> q0\n";
                        } else{
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 3:
                        if (w == 'a') {
                            estado = 3;
                            retorno = retorno + "q3 -> q3\n";
                        } else if(w == 'b'){
                            estado = 3;
                            retorno = retorno + "q3 -> q3\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 4:
                        if (w == 'a') {
                            estado = 1;
                            retorno = retorno + "q4 -> q1\n";
                        } else if(w == 'b'){
                            estado = 4;
                            retorno = retorno + "q4 -> q4\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                }
                if (erro == true){
                    return "Erro - caracteres não aceitos";
                }
                pos++;
                if (pos == entrada.length()) {
                    terminou = true;
                }
            }
            if (estado == 3) {
                retorno = retorno + "Entrada aceita";
            } else {
                retorno = retorno + "Condição não aceita";
            }
        }
        return retorno;
    }
    
    public static String C(String entrada) {
        String retorno = "Exercicio c\n"
                + "E1 = {a, b}\n"
                + "L3= { w e E1* | w possui baba como prefixo e abab como sufixo }\n";

        char w;
        
        boolean terminou = false;
        boolean erro = false;
        
        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA";
        } 
            retorno = retorno + "ENTRADA: " + entrada + "\n";
            while (!terminou) {
                w = entrada.charAt(pos);

                switch (estado) {
                    case 0:
                        if (w == 'b') {
                            estado = 1;
                            retorno = retorno + "q0 -> q1\n";
                        } else if (w == 'a') {
                            estado = 9;
                            retorno = retorno + "q0 -> q9\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 1:
                        if (w == 'a') {
                            estado = 2;
                            retorno = retorno + "q1 -> q2\n";
                        } else if (w == 'b') {
                            estado = 9;
                            retorno = retorno + "q1 -> q9\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 2:
                        if (w == 'b') {
                            estado = 3;
                            retorno = retorno + "q2 -> q3\n";
                        } else if (w == 'a') {
                            estado = 9;
                            retorno = retorno + "q2 -> q9\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 3:
                        if (w == 'a') {
                            estado = 4;
                            retorno = retorno + "q3 -> q4\n";
                        } else if (w == 'b') {
                            estado = 9;
                            retorno = retorno + "q3 -> q9\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 4:
                        if (w == 'b') {
//                            estado = 4;
//                            System.out.println("q4 -> q4\n");
                        } else if (w == 'a') {
                            estado = 5;
                            retorno = retorno + "q4 -> q5\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 5:
                        if (w == 'b') {
                            estado = 6;
                            retorno = retorno + "q5 -> q6\n";
                        } else if (w == 'a') {
//                            estado = 5;
//                            System.out.println("q5 -> q5\n");
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 6:
                        if (w == 'a') {
                            estado = 7;
                            retorno = retorno + "q6 -> q7\n";
                        } else if (w == 'b') {
                            estado = 4;
                            retorno = retorno + "q6 -> q4\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 7:
                        if (w == 'b') {
                            estado = 8;
                            retorno = retorno + "q7 -> q8\n";
                        } else if (w == 'a') {
                            estado = 5;
                            retorno = retorno + "q7 -> q5\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;    
                    case 8:
                        if (w == 'b') {
                            estado = 4;
                            retorno = retorno + "q8 -> q4\n";
                        } else if (w == 'a') {
                            estado = 5;
                            retorno = retorno + "q8 -> q5\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 9:
 						if (w == 'a' || w == 'b'){
//                      	  estado = 9;
//                      	  retorno = retorno +"q9 -> q9\n");
						} else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                }
                if (erro == true){
                    return "Erro - caractere não aceito: ' " + w + " '";
                }
                pos++;
                if (pos >= entrada.length()) {
                    terminou = true;
                }
            }
            if (estado == 0 || estado == 8) {
                retorno = retorno + "Entrada aceita\n";
            } else {
                retorno = retorno + "Condição não aceita\n";
            }
        return retorno;
        
    }
    
    public static String D(String entrada) {
        String retorno = "Exercicio d\n"
                + "E1 = {a, b}\n"
                + "L4= { w e E1* | w possui no máximo uma ocorrência da cadeia baba}\n";

        char w;
        
        boolean terminou = false;
        boolean erro = false;

        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA";
        } 
            retorno = retorno +"ENTRADA: " + entrada + "\n";
            while (!terminou) {
                w = entrada.charAt(pos);

                switch (estado) {
                    case 0:
                        if (w == 'a') {
//                            estado = 0;
//                            retorno = retorno +"q0 -> q0\n";
                        } else if (w == 'b') {
                            estado = 1;
                            retorno = retorno +"q0 -> q1\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 1:
                        if (w == 'a') {
                            estado = 2;
                            retorno = retorno +"q1 -> q2\n";
                        } else if (w == 'b') {
//                            estado = 1;
//                            retorno = retorno +"q1 -> q1";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 2:
                        if (w == 'a') {
                            estado = 0;
                            retorno = retorno +"q2 -> q0\n";
                        } else if (w == 'b') {
                            estado = 3;
                            retorno = retorno +"q2 -> q3\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 3:
                        if (w == 'a') {
                            estado = 4;
                            retorno = retorno +"q3 -> q4\n";
                        } else if (w == 'b'){
                            estado = 1;
                            retorno = retorno +"q3 -> q1\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 4:
                        if (w == 'a') {
                            estado = 7;
                            retorno = retorno +"q4 -> q7\n";
                        } else if (w == 'b') {
                            estado = 5;
                            retorno = retorno +"q4 -> q5\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 5:
                        if (w == 'a') {
                            estado = 6;
                            retorno = retorno +"q5 -> q6\n";
                        } else if (w == 'b') {
                            estado = 8;
                            retorno = retorno +"q5 -> q8\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 6:
                        if (w == 'a' || w == 'b') {
//                            estado = 6;
//                            retorno = retorno +"q6 -> q6\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 7:
                        if (w == 'a') {
//                            estado = 7;
//                            retorno = retorno +"q7 -> q7\n";
                        } else if (w == 'b') {
                            estado = 8;
                            retorno = retorno +"q7 -> q8\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 8:
                        if (w == 'a') {
                            estado = 9;
                            retorno = retorno +"q7 -> q9\n";
                        } else if (w == 'b') {
//                            estado = 8;
//                            retorno = retorno +"q8 -> q8\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 9:
                        if (w == 'a') {
                            estado = 7;
                            retorno = retorno +"q9 -> q7\n";
                        } else if (w == 'b') {
                            estado = 10;
                            retorno = retorno +"q9 -> q10\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                        
                    case 10:
                        if (w == 'a') {
                            estado = 11;
                            retorno = retorno +"q10 -> q11\n";
                        } else if (w == 'b') {
                            estado = 8;
                            retorno = retorno +"q10 -> q8\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break;
                    case 11:
                        if (w == 'a' || w == 'b') {
//                            estado = 11;
//                            retorno = retorno +"q11 -> q11\n";
                        } else {
                            terminou = true;
                            erro = true;
                        }
                        break; 
                    
                }
                if (erro == true){
                    return "Erro - caractere não aceito:  ' " + w + " '";
                }
                pos++;
                if (pos >= entrada.length()) {
                    terminou = true;
                }
            }
            if (estado != 8) {
                retorno = retorno +"Entrada aceita\n";
            } else {
                retorno = retorno +"Condição não aceita\n";
            }
        return retorno;
    }
    
    public static String E(String entrada) {
        String retorno = "Exercicio E\n"
                + "E1 = {a, b}\n"
                + "L5= { w e E1* | w não possui ocorrência da cadeia baba}\n";

        char w;
        boolean erro = false;
        int estado = 0;
        
        if(entrada.length()==0){
            retorno = retorno + "ENTRADA : VAZIA\n";
        }else{
            if(!entrada.matches("[a|b][a|b]+")){
                retorno = retorno+"Entrada possui carateres não aceitos!\n";
            }else{
            
                        for(int i = 0;i<entrada.length();i++){
            w = entrada.charAt(i);
            if(w!='a' && w!='b'){
                retorno = retorno+"Entrada possui caracteres não aceitos!";
            }
            else{
                switch(estado){
                    case 0:
                        if(w=='a'){
                            retorno = retorno+ "q0 -> q0\n"; 
                        }else{
                            retorno = retorno+ "q0 -> q1\n";
                            estado = 1;
                        }
                        break;
                    case 1:
                        if(w=='a'){
                            retorno = retorno +"q1 -> q2\n";
                            estado = 2;
                        }else{
                            retorno = retorno +"q1 -> q1\n";
                            estado = 1;
                        }
                        break;
                    case 2:
                        if(w=='a'){
                            retorno = retorno +"q2 -> q0\n";
                            estado = 0;
                        }else{
                            retorno = retorno +"q2 -> q3\n";
                            estado = 3;
                            
                        }
                        break;
                    case 3:
                        if(w=='a'){
                            retorno = retorno + "q3 -> q4 : Erro\n";
                            estado = 4;
                        }else{
                            retorno = retorno + "q3 -> q0\n";
                            estado =0;
                        }
                        break;
                    case 4:
                        retorno = retorno + "q4 -> q4\n";
                        estado =4;
                        break;                            
                }
            
            }

        }
        
        if(estado!=4){
            retorno = retorno +"Entrada aceita!\n";
        }else{
            retorno = retorno+"Entrada não aceita!\n";
        }
            }
                
              
                
        }
            
        return retorno;
        
        //TODO Exercicio E
    }
    
    public static String F(String entrada) {
        String retorno = "Exercicio F\n"
                + "E1 = {a, b}\n"
                + "L6= { w e E1* | w possui ocorrência par de a 's seguida por ocorrência impar de b 's}\n";

        char w;
        boolean erro = false;
        int estado = 0;
        
        if(entrada.length()==0){
            retorno = retorno + "ENTRADA VAZIA!";
        }else{
            if(!entrada.matches("[a|b][a|b]+")){
                retorno = retorno+"Entrada possui carateres não aceitos!\n";
                
            }else{
                        for(int i = 0;i<entrada.length();i++){
            w = entrada.charAt(i);
            if(w!='a' && w!='b'){
                retorno = String.format(retorno+"Entrada possui carateres não aceitos: %c\n", w);
                break;
            }
            else{
                switch(estado){
                    case 0:
                        if(w=='a'){
                            retorno = retorno + "q0 -> q2\n";
                            estado = 2;
                        }else{
                            retorno = retorno + "q0 -> q1 : Erro";
                            estado = 1;
                        }
                        break;
                    case 1:
                        retorno = retorno + "q1 -> q1\n";
                        estado = 1;
                        
                        break;
                    case 2:
                        if(w=='a'){
                            retorno = retorno + "q2 -> q3\n";
                            estado = 3;
                        }else{
                            retorno = retorno + "q2 -> q1\n";
                            estado = 1;
                        }
                        break;
                    case 3:
                        if(w=='a'){
                            retorno = retorno + "q3 -> q2\n";
                            estado = 2;
                        }else{
                            retorno = retorno + "q3 - > q4\n";
                            estado = 4;
                        }
                        break;
                    case 4:
                        if(w=='a'){
                            retorno = retorno + "q4 - > q1\n";
                            estado = 1;
                        }else{
                            retorno = retorno + "q4 - > q5\n";
                            estado = 5;
                        }
                        break;                            
                    case 5:
                        if(w=='a'){
                            retorno = retorno + "q5 - > q1\n";
                            estado = 1;
                        }else{
                            retorno = retorno + "q5 - > q4\n";
                            estado = 4;
                            
                        }
                        
                        break;
                        
                }
                
                
                
            }
           
            
        }
        
        if(estado != 4){
            retorno = retorno + "Entrada Não Aceita!\n";
        }else{
            retorno = retorno + "Entrada Aceita!\n";
        }
               
        }
       }
        
        return retorno;
        
        //TODO Exercicio F
    }
    
    public static String G(String entrada) {
        String retorno = "Exercicio g \n"
                + "E2 = {a, b, c, ... ,z}\n"
                + "L7 = {w  e E2* | w começa com a e possui ocorrência par de a 's ou começa com b e\n"
                + "possui ocorrência impar de b 's }\n\n";
        char w;

        boolean terminou = false;
        boolean erro = false;
        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA -> Condição não aceita";
            retorno = retorno + "ENTRADA: " + entrada + "\n";
        } else {
            while (!terminou) {
                w = entrada.charAt(pos);

                switch (estado) {
                    case 0:
                        if (w > 'b') {
                            estado = 3;
                            retorno = retorno + ("q0 -> q3 Erro\n");
                        } else if (w == 'a') {
                            estado = 1;
                            retorno = retorno + ("q0 -> q1\n");
                        } else if (w == 'b') {
                            estado = 4;
                            retorno = retorno + ("q0 -> q4\n");
                        } else if (w < 'a' || w > 'z'){
                            erro = true;
                        }
                        break;
                    case 1:
                        if (w >= 'b') {
                            estado = 1;
                            retorno = retorno + ("q1 -> q1\n");
                        } else if (w == 'a') {
                            estado = 2;
                            retorno = retorno + ("q1 -> q2\n");
                        } else if (w < 'a' || w > 'z'){
                            erro = true;
                        }
                        break;
                    case 2:
                        if (w >= 'b') {
                            estado = 2;
                            retorno = retorno + ("q2 -> q2\n");
                        } else if (w == 'a') {
                            estado = 1;
                            retorno = retorno + ("q2 -> q1\n");
                        } else if (w < 'a' || w > 'z'){
                            erro = true;
                        }
                        break;
                    case 3:
                        if (w >= 'a') {
                            estado = 3;
                            retorno = retorno + ("q3 -> q3 Erro\n");
                        }
                        break;
                    case 4:
                        if (w == 'a' || w >= 'c') {
                            estado = 4;
                            retorno = retorno + ("q4 -> q4\n");
                        } else if (w == 'b') {
                            estado = 5;
                            retorno = retorno + ("q4 -> q5\n");
                        } else if (w < 'a' || w > 'z'){
                            erro = true;
                        }
                        break;
                    case 5:
                        if (w == 'b') {
                            estado = 4;
                            retorno = retorno + ("q5 -> q4\n");
                        } else if (w == 'a' || w >= 'c') {
                            estado = 5;
                            retorno = retorno + ("q5 -> q5\n");
                        } else if (w < 'a' || w > 'z'){
                            erro = true;
                        }
                        break;
                }
                if (erro) {
                    return "Erro - caracteres não aceitos";
                }
                pos++;
                if (pos == entrada.length()) {
                    terminou = true;
                }
            }
            if (estado == 2 || estado == 4) {
                retorno = retorno + ("Entrada aceita");
            } else {
                retorno = retorno + ("Condição não aceita");
            }
        }
        return retorno;
    }

    public static String H(String entrada) {
        String retorno = "Exercicio h \n"
                + "E3 = {0, 1, 2, ... , 9}\n"
                + "L8 = {w  e E3* | w inicia-se com 0 e a soma de todos os seus dígitos é par, ou inicia-se\n"
                + "com 1 e a soma de todos os seus dígitos é impar }\n\n";
        char w;

        boolean terminou = false;
        boolean erro = false;
        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA -> Condição não aceita";
            retorno = retorno + "ENTRADA: " + entrada + "\n";
        } else {
            while (!terminou) {
                w = entrada.charAt(pos);

                switch (estado) {
                    case 0:
                        if (w == '0' || w == '1') {
                            estado = 1;
                            retorno = retorno + ("q0 -> q1\n");
                        } else if (w >= '2') {
                            estado = 3;
                            retorno = retorno + ("q0 -> q3 Erro\n");
                        } else if (w > '9' || w < '0'){
                            erro = true;
                        }
                        break;
                    case 1:
                        if (w == '0' || w == '2' || w == '4' || w == '6' || w == '8') {
                            estado = 1;
                            retorno = retorno + ("q1 -> q1\n");
                        } else if (w == '1' || w == '3' || w == '5' || w == '7' || w == '9') {
                            estado = 2;
                            retorno = retorno + ("q1 -> q2\n");
                        }  else if (w > '9' || w < '0'){
                            erro = true;
                        }
                        break;
                    case 2:
                        if (w == '0' || w == '2' || w == '4' || w == '6' || w == '8') {
                            estado = 2;
                            retorno = retorno + ("q2 -> q2\n");
                        } else if (w == '1' || w == '3' || w == '5' || w == '7' || w == '9') {
                            estado = 1;
                            retorno = retorno + ("q2 -> q1\n");
                        }  else if (w > '9' || w < '0'){
                            erro = true;
                        }
                        break;
                    case 3:
                        if (w >= '0') {
                            estado = 3;
                            retorno = retorno + ("q3 -> q3 Erro\n");
                        }  else if (w > '9' || w < '0'){
                            erro = true;
                        }
                        break;
                }
                if (erro) {
                    return "Erro - caracteres não aceitos";
                }
                pos++;
                if (pos == entrada.length()) {
                    terminou = true;
                }
            }
            if (estado == 1) {
                retorno = retorno + ("Entrada aceita");
            } else {
                retorno = retorno + ("Condição não aceita");
            }
        }
        return retorno;
    }

    public static String I(String entrada) {
         
        String retorno = "Exercicio I\n"
                        + "E2 = {a, b, c, ... ,z}\n"
                        + "E3 = {0, 1, 2, ... , 9}\n"
                        + "L9=  { w e E2 U E3)* | w inicia-se com uma letra, possuindo a seguir qualquer\n" 
                        +"combinação de letras e dígitos}\n";

        String w;
     
        boolean erro = false;

        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            entrada = "VAZIA";
            retorno = retorno +"ENTRADA: "+entrada;
        } else{
            if(!entrada.matches("[\\w|\\d][\\w|\\d]+")){
                retorno = retorno+"Entrada possui carateres não aceitos!\n";
            }else{
                        for(int i = 0;i<entrada.length();i++){
                w = String.format("%c", entrada.charAt(i));
                if(!w.matches("[\\w|\\d]")){
                    
                    break;
                }
                else{
                switch(estado){
                    case 0:
                        if(w.matches("[a-z]")){
                            retorno = retorno + "q0 -> q1\n";
                            estado = 1;
                        }else{
                            retorno = retorno + "q0 -> q2 : Erro\n";
                            estado = 2;
                        }
                            
                        break;
                    case 1:
                       
                            retorno = retorno + "q1 -> q1\n";
                            estado = 1;
                        
                       
                        break;
                    case 2:
                        retorno = retorno + "q2 -> q2\n";
                        estado = 2;
                        break;
                        
                }
            }
        }
        
        if(estado == 1){
            retorno = retorno + "Entrada Aceita!\n";
        }else{
            retorno = retorno + "Entrada não Aceita\n";
        }
            
        }
        

            
        
        }
        
        return retorno;
        
    }
    
    public static String J(String entrada) {
        String retorno = "Exercicio J\n"
                + "E3 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}\n"
                + "L4= { w e E3* U {+, -, e, ','} | w é um número inteiro, um número decimal ou um número representado em notação científica}\n";

        char w;

        boolean terminou = false;
        boolean erro = false;
        
        int estado = 0;
        int pos = 0;
        if (entrada.length() == 0) {
            terminou = true;
            entrada = "VAZIA";
        }
        retorno = retorno + "ENTRADA: " + entrada + "\n";
        while (!terminou) {
            w = entrada.charAt(pos);

            switch (estado) {
                case 0:
                    if (w == '+' || w == '-') {
                        estado = 1;
                        System.out.println("q0 -> q1");
                    } else if(w == 'e' || w == ',' || (w > '0' && w < '9' )) {
                        estado = 7;
                        System.out.println("q0 -> q7");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 1:
                    if (w == '+' || w == '-' || w == 'e' || w == ',') {
                        estado = 7;
                        System.out.println("q1 -> q7");
                    } else if (w >= '0' && w <= '9' ) {
                        estado = 2;
                        System.out.println("q1 -> q2");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 2:
                    if (w == 'e') {
                        estado = 5;
                        System.out.println("q2 -> q5");
                    } else if (w == ',') {
                        estado = 3;
                        System.out.println("q2 -> q3");
                    } else if (w == '+' || w == '-') {
                        estado = 7;
                        System.out.println("q2 -> q7");
                    } else if (w >= '0' && w <= '9') {
//                            estado = 2;
//                            System.out.println("q2 -> q2");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 3:
                    if (w == '+' || w == '-' || w == 'e' || w == ',') {
                        estado = 7;
                        System.out.println("q3 -> q7");
                    } else if(w >= '0' && w <= '9') {
                        estado = 4;
                        System.out.println("q3 -> q4");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 4:
                    if (w == 'e') {
                        estado = 5;
                        System.out.println("q4 -> q5");
                    } else if (w == '+' || w == '-' || w == ',') {
                        estado = 7;
                        System.out.println("q4 -> q7");
                    } else if(w >= '0' && w <= '9') {
//                            estado = 2;
//                            System.out.println("q2 -> q2");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 5:
                    if (w == '+' || w == '-' || w == 'e' || w == ',') {
                        estado = 7;
                        System.out.println("q5 -> q7");
                    } else if(w >= '0' && w <= '9') {
                        estado = 6;
                        System.out.println("q5 -> q6");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 6:
                    if (w == '+' || w == '-' || w == 'e' || w == ',') {
                        estado = 7;
                        System.out.println("q6 -> q7");
                    } else if (w >= '0' && w <= '9' ) {
//                            estado = 6;
//                            System.out.println("q6 -> q6");
                    } else {
                        terminou = true;
                        erro = true;
                    }
                    break;
                case 7:
//                        estado = 7;
//                        System.out.println("q7 -> q7");
                    break;
            }
            if (erro == true){
                return "Erro - caractere não aceito: ' " + w + " '";
            }
            pos++;
            if (pos >= entrada.length()) {
                terminou = true;
            }
        }
        if (estado == 2 || estado == 4 || estado == 6) {
            retorno = retorno + "Entrada aceita!!\n";
        } else {
            retorno = retorno + "Condição não aceita\n";
        }
        return retorno;
    }
}